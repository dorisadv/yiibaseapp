<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property integer $status
 * @property string $memo
 * @property string $date
 */
class Orders extends CActiveRecord
{
    const TYPE_CALL = 0;
    const TYPE_DOCS = 1;
    const TYPE_MONEY = 2;
    const TYPE_PROCESS = 3;
    const TYPE_FOOTER = 4;
    
    const STATUS_NEW = 0;
    const STATUS_PROCESSED = 1;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Orders the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'orders';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, name, email, phone, status', 'required'),
            array('type, status', 'numerical', 'integerOnly' => true),
            array('name, memo', 'length', 'max' => 512),
            array('email', 'length', 'max' => 128),
            array('phone', 'length', 'max' => 64),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, type, name, email, phone, status, memo, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Тип',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'memo' => 'Примечание',
            'date' => 'Дата',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('type', $this->type);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('memo', $this->memo, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public static function getType($type=NULL)
    {
        $types = array(
            self::TYPE_CALL => 'Заказ звонка',
            self::TYPE_DOCS => 'Счетчик',
            self::TYPE_MONEY => 'Заказ согласования',
            self::TYPE_PROCESS => 'Заказ проекта',
            self::TYPE_FOOTER => 'Новый клиент'
        );
        if($type!==NULL) {
            
            return isset($types[$type])? $types[$type] : NULL;
        } else {
            
            return $types;
        }
    }
    public static function getStatus($status=NULL)
    {
        $statuses = array(
            self::STATUS_NEW => 'Новый',
            self::STATUS_PROCESSED => 'Обработан'
        );
        if($status!==NULL) {
            
            return isset($statuses[$status])? $statuses[$status] : NULL;
        } else {
            
            return $statuses;
        }
    }

}