<?php

/**
 * This is the model class for table "portfolio".
 *
 * The followings are the available columns in table 'portfolio':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $images
 * @property integer $sort
 * @property integer $status
 * @property string $date
 */
class Portfolio extends ActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className status record class name.
     * @return Portfolio the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'portfolio';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, images', 'required'),
            array('sort, status', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 128),
            array('description, images', 'length', 'max' => 4096),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, description, images, sort, status, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'images' => 'Изображения',
            'sort' => 'Сортировка',
            'status' => 'Статус',
            'date' => 'Дата модификации',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('images', $this->images, true);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('status', $this->status);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function scopes()
    {
        return array(
            'published' => array(
                'condition' => 'status=1',
                'order' => 'sort'
            )
        );
    }

    public function getMainImage()
    {
        if(!empty($this->images)) {
            $images = explode(',', $this->images);
            return array_shift($images);
        }
        return '';
    }
}