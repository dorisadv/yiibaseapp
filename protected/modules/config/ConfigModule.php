<?php

class ConfigModule extends CWebModule
{

    public function init()
    {
//        $this->install();

        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'config.models.*',
            'config.helpers.*'
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {

            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else {

            return false;
        }
    }

    /**
     * @todo install table in DB
     */
    public function install()
    {
        Yii::app()->db->createCommand()->createTable('config', array(
            'id' => 'pk',
            'module' => 'varchar(32) NOT NULL',
            'varname' => 'varchar(32) NOT NULL',
            'vartype' => 'varchar(16) NOT NULL',
            'varvalue' => 'varchar(4096) NOT NULL',
            'params' => 'varchar(256) NULL',
            'sort' => 'int(11) DEFAULT "1" NOT NULL',
            'date' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ), 'ENGINE=MyISAM');
    }

}
