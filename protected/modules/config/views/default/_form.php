<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'config-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true, 
    )
));
?>
<?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldRow($model, 'module', array('class' => 'span5', 'maxlength' => 32)); ?>
<?php echo $form->textFieldRow($model, 'varname', array('class' => 'span5', 'maxlength' => 32)); ?>
<?php echo $form->dropDownListRow($model, 'vartype', Config::getTypes()); ?>
<?php echo $form->textFieldRow($model, 'params', array('class' => 'span5', 'maxlength' => 256)); ?>    
<?php echo $form->textFieldRow($model, 'varvalue', array('class' => 'span5', 'maxlength' => 8192)); ?>    
<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => Yii::t('configModule.common', ($model->isNewRecord ? 'Add' : 'Save')),
    ));
    ?>
</div>
<?php $this->endWidget(); ?>
