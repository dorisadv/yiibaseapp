<table class="table table-config table-hover table-bordered">
    <thead>
        <tr>
            <th><?php echo Yii::t('configModule.common', 'Var name'); ?></th>
            <th><?php echo Yii::t('configModule.common', 'Var value'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($configObj as $cObj) { ?>
            <tr>
                <td><?php echo Yii::t('configModule.settings', $cObj->varname); ?></td>
                <td><?php echo $cObj->editView(); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php 
if(!empty($info)) {
    
    echo CHtml::tag('div', array('class'=>'alert alert-info nomargin', 'id'=>$module.'info'), $info);
}
?>