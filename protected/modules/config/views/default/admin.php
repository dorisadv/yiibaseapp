<h1>Управление конфигом</h1>
<div class="actions">
    <?php echo CHtml::link('Добавить конфиг', array('/config/default/create'), array('class' => 'btn btn-success')); ?>    
</div>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'config-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array('width' => '50px')
        ),
        'module',
        'varname',
        'vartype',
        'varvalue',
//        'date',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}'
        ),
    ),
));
?>
