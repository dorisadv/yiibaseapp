<h1>Управление настройками</h1>
<?php if(!empty($tabs)) { ?>
<div class="form">
    <?php echo CHtml::beginForm(); ?>  
    <?php
    $this->widget('bootstrap.widgets.TbTabs', array(
        'type' => 'tabs',
        'placement' => 'top',
        'tabs' => $tabs,
    ));
    ?>
    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => Yii::t('configModule.common', 'Save')
        ));
        ?>
    </div>   
    <?php echo CHtml::endForm(); ?>
</div><!-- form -->
<?php } else { ?>
    Настроек пока нет. <?php echo CHtml::link('Добавить', array('/config/default/admin')); ?>
<?php } ?>