<?php

class EConfig extends CApplicationComponent
{    
    protected static $config = NULL;

    public function init()
    {
        parent::init();        
        
        self::$config = array();
        $config = Config::getConfig();
        foreach ($config as $module => $moduleArr) {
            
            foreach($moduleArr as $cfg) {

                self::$config[$module][$cfg->varname] = $cfg->varvalue;
            }
        }
    }        
    
    public function get($module, $key=NULL)
    {
        if($key !== NULL) {
            
            return (isset(self::$config[$module][$key]))? self::$config[$module][$key] : NULL;
        } else {
            
            return (isset(self::$config[$module]))? self::$config[$module] : array();
        }
    }

}

?>