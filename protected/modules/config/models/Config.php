<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property integer $id
 * @property string $module
 * @property string $varname
 * @property string $vartype
 * @property string $params
 * @property string $varvalue
 * @property integer $sort
 * @property string $date
 */
class Config extends CActiveRecord
{

    const TYPE_ARRAY = 'arr';
    const TYPE_BOOLEAN = 'bool';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_HTML = 'html';
    const TYPE_DATE = 'date';
    const TYPE_DROPBOX = 'select';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Config the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'config';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('module, varname, vartype', 'required'),
            array('sort', 'numerical', 'integerOnly'=>true),
            array('module, varname', 'length', 'max' => 32),
            array('vartype', 'length', 'max' => 16),
            array('params', 'length', 'max' => 256),
            array('varvalue', 'length', 'max' => 8192),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, module, varname, vartype, varvalue, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'module' => 'Модуль',
            'varname' => 'Навание',
            'vartype' => 'Тип',
            'varvalue' => 'Значение',
            'params' => 'Параметры',
            'sort' => 'Сортировка',
            'date' => 'Дата модификации',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('module', $this->module, true);
        $criteria->compare('varname', $this->varname, true);
        $criteria->compare('vartype', $this->vartype, true);
        $criteria->compare('varvalue', $this->varvalue, true);
        $criteria->compare('sort',$this->sort);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    } 
    
    public function defaultScope()
    {
        return array(
            'order' => 'sort, id'
        );
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {

            $this->varvalue = self::prepareDBValue($this->varvalue, $this->vartype);
            return true;
        } else {

            return false;
        }
    }

    protected function afterFind()
    {
        parent::afterFind();
        $this->varvalue = self::prepareDBValue($this->varvalue, $this->vartype);
    }

    public function editView()
    {
        $this->params = !empty($this->params)? json_decode($this->params, TRUE) : array();
        return self::editViewHtml($this->id, $this->varvalue, $this->vartype, $this->params);
    }

    public static function getConfig(Array $modules = array())
    {
        $config = array();
        $criteria = new CDbCriteria();
        if(!empty($modules)) {
            
            $criteria->addInCondition('module', $modules);
        }
        $configAll = self::model()->findAll($criteria);

        foreach ($configAll as $configObj) {

            $config[$configObj->module][] = $configObj;
        }

        return $config;
    }

    public static function prepareDBValue($value, $type)
    {
        switch ($type) {

            case self::TYPE_ARRAY:
                if (is_string($value)) {

                    $value = json_decode($value, TRUE);
                } else {

                    $value = json_encode($value);
                }
                break;

            case self::TYPE_BOOLEAN:
                $value = !empty($value) ? 1 : 0;
                break;
        }
        return $value;
    }

    public static function editViewHtml($id, $value, $type, Array $params = array())
    {
        $html = '';
        $name = 'config[' . $id . ']';
        
        ob_start();        
        switch ($type) {

            case self::TYPE_BOOLEAN:
                echo CHtml::hiddenField($name, 0);
                echo CHtml::checkBox($name, !empty($value));
                break;
            case self::TYPE_STRING:
                echo CHtml::textField($name, $value);
                break;
            case self::TYPE_TEXT:
                echo CHtml::textArea($name, $value, array('class'=>'span5'));
                break;
            case self::TYPE_DATE:                
                Yii::app()->controller->widget('configModuleExt.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'language'=>'ru',   
                    'name' => $name,
                    'value' => $value,
                    'mode'=>'datetime',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                        'timeFormat' => 'hh:mm:00',
                    )
                ));                
//                Yii::app()->clientScript->registerScript('config-date', '
//                    jQuery(function(){ $("input.hasDatepicker").attr("disabled","disabled"); });
//                ');
                break;
            case self::TYPE_HTML:
                Yii::app()->controller->widget('configModuleExt.redactor.Redactor', array(                    
                    'name' => $name,
                    'value' => $value,
                    'options' => array(
                        'lang' => 'ru',
                    ),
                ));                
                break;
            case self::TYPE_DROPBOX:
                echo CHtml::dropDownList($name, $value, $params);
                break;
        }
        
        $html .= ob_get_clean();
        return $html;
    }
    
    public static function getTypes()
    {
        return array(
            self::TYPE_STRING => 'String',
            self::TYPE_TEXT => 'Text',
            self::TYPE_HTML => 'Html',
            self::TYPE_ARRAY => 'Array',
            self::TYPE_BOOLEAN => 'Boolean',
            self::TYPE_DATE => 'Date',
        );
    }

}