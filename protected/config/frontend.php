<?php

return CMap::mergeArray(
        
    array(
        'name' => 'YiiBaseApp',
        'components' => array(            
            'urlManager' => array(
                'rules' => array(
                    '/' => 'site/index',                    
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                )
            ),
            'errorHandler' => array(
                'errorAction' => 'site/error',
            ),
            'request'=>array(
                'enableCsrfValidation'=>true,
            ),
        )
    ), require(dirname(__FILE__) . '/main.php')
);