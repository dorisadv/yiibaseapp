<?php

Yii::setPathOfAlias('widgets', dirname(__FILE__).'/../components/widgets');
Yii::setPathOfAlias('configModule', dirname(__FILE__).'/../modules/config');
Yii::setPathOfAlias('configModuleExt', dirname(__FILE__).'/../modules/config/extensions');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',    
    'preload' => array('log'),
    'sourceLanguage'=>'en',        
    'language' => 'ru',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.components.actions.*',
        'application.components.CImageHandler',
        'configModule.models.*'
    ),
    'behaviors' => array(
        'runEnd' => array(
            'class' => 'application.behaviors.WebApplicationEndBehavior',
        ),
    ),    
    'components' => array(
        'ih'=>array(
            'class'=>'CImageHandler',
        ),
        'config' => array(
            'class'=>'configModuleExt.config.EConfig',
        ),
        'user' => array(
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName'=>false,            
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=yiibaseapp',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),  
        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.views.email',
            'pathLayouts' => 'application.views.email.layouts'
        ),
//        'log' => array(
//            'class' => 'CLogRouter',
//            'routes' => array(
//                array(
//                    'class' => 'CFileLogRoute',
//                    'levels' => 'error, warning',
//                ),
//                array(
//                    'class' => 'CWebLogRoute',
//                ),
//            ),
//        )
    ),
    'params' => array(
        'adminEmail' => 'iov.mail.zt@gmail.com',
    ),
);