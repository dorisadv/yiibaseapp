<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');

return CMap::mergeArray(
        
    array(        
        'defaultController' => 'admin',
        'homeUrl'=> array('admin/index'),
        'name' => 'YiiBaseApp ПанельУправления',
        'import' => array(
            'editable.*'
        ),
        'modules' => array(            
            'gii' => array(                
                'class' => 'system.gii.GiiModule',
                'password' => 'hakkerok',
                'ipFilters' => array('127.0.0.1', '::1'),
                'generatorPaths'=>array(
                    'bootstrap.gii',
                ),
            ),
            'config' => array(),
        ),
        'components' => array(            
            'user' => array(
                'loginUrl' => array('admin/login'),
                'allowAutoLogin' => true,
            ),
            'urlManager' => array(
                'rules' => array(                         
                    'admin/gii'=>'gii',
                    'admin/gii/<controller:\w+>'=>'gii/<controller>',
                    'admin/gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',                    
                    'admin/' => 'admin/index',                    
                    'admin/<action:\w+>' => 'admin/<action>',                    
                    'admin/<controller:\w+>/<id:\d+>' => '<controller>/view',
                    'admin/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    'admin/<controller:\w+>/<action:\w+>' => '<controller>/<action>',                    
                    'admin/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>', 
                )
            ),
            'bootstrap'=>array(
                'class'=>'bootstrap.components.Bootstrap',
            ),
            'editable' => array(
                'class'     => 'editable.EditableConfig',
                'form'      => 'bootstrap',        //form style: 'bootstrap', 'jqueryui', 'plain' 
                'mode'      => 'popup',            //mode: 'popup' or 'inline'  
                'defaults'  => array(              //default settings for all editable elements
                   'emptytext' => 'Редактировать'
                )
            ),
            'errorHandler' => array(
                'errorAction' => 'admin/error',
            ),
            'user' => array(
                'loginUrl' => array('/admin/login')
            )
        )
    ), require(dirname(__FILE__) . '/main.php')
);