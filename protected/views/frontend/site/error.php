<div class="error block-margin">
    <h2>Error <?php echo $code; ?></h2>    
    <p><?php echo CHtml::encode($message); ?></p>
</div>