<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/stylesheet.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>
    <body>
        <div id="wrap"> 
            <div id="header">
                <!--BOF HEADER-->
                <div class="header-block">                    
                    <div class="container">
                        HEADER
                    </div>
                </div>
                <!--EOF HEADER-->                
            </div>            
            <div id="main" class="clearfix">
                <?php echo $content; ?>
            </div>
        </div>
        <div id="footer">
            <!--BOF - FOOTER-->
            <div class="footer-block">
                <div class="container">
                    FOOTER
                </div>
            </div>
            <!--EOF - FOOTER-->
        </div>        
    </body>
</html>