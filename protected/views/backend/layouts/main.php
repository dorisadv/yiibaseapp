<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />       
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php Yii::app()->bootstrap->register(); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/admin.css" />
    </head>
    <body>
        <div id="wrap">
            <div id="header">
                <?php
                $this->widget('bootstrap.widgets.TbNavbar', array(
                    'brandUrl' => $this->createAbsoluteUrl('/admin/index'),
                    'items' => array(
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'items' => array(                                
                                array('label' => 'Настройки', 'url' => array('/config/default/index'), 'visible' => !Yii::app()->user->isGuest),
                                array('label' => 'Войти', 'url' => array('/admin/login'), 'visible' => Yii::app()->user->isGuest),
                                array('label' => 'Выйти (' . Yii::app()->user->name . ')', 'url' => array('/admin/logout'), 'visible' => !Yii::app()->user->isGuest)
                            ),
                        ),
                    ),
                ));
                ?>
            </div>
            <div class="container clearfix" id="page">                
                <?php echo $content; ?>               
            </div><!-- page -->
        </div>
        <div id="footer" class="navbar-inner">
            <div class="container">
                <span class="autor">YiiBaseApp</span>
                <?php echo CHtml::link('На сайт', '/'); ?>
                <span class="copyright">Copyright &copy; <?php echo date('Y'); ?></span>
            </div>
        </div><!-- footer -->
    </body>
</html>
