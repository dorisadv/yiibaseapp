<?php
/* @var $this AdminController */
/* @var $model AdminLoginForm */
/* @var $form CActiveForm  */
$this->pageTitle = Yii::app()->name . ' - Авторизация';
?>
<div class="login-block">
    <h1>Авторизация</h1>
    <div class="form">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'login-form',
            'type' => 'horizontal',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>   
        <?php echo $form->textFieldRow($model, 'username'); ?>
        <?php echo $form->passwordFieldRow($model, 'password'); ?>
        <?php echo $form->checkBoxRow($model, 'rememberMe'); ?>
        <div class="form-actions">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => 'Войти',
            ));
            ?>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div>
