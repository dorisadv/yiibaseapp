<?php

class ActiveRecord extends CActiveRecord
{

    /**
     * @ Param Criteria $ CDbCriteria 
     * @ Return CActiveDataProvider 
     */
    public function getDataProvider(Array $settings = array())
    {
        $criteria = isset($settings['criteria'])? $settings['criteria'] : null;
        $pagination = isset($settings['pagination'])? $settings['pagination'] : null;
        unset($settings['criteria']);
        unset($settings['pagination']);
        
        if ((is_array($criteria)) || ($criteria instanceof CDbCriteria)) {
            
            $this->getDbCriteria()->mergeWith($criteria);
        }        
        $pagination = CMap::mergeArray(array('pageSize' => 9), (array) $pagination);
        
        return new CActiveDataProvider(get_called_class(), $settings + array(
            'criteria' => $this->getDbCriteria(),
            'pagination' => $pagination
        ));
    }

}