<?php
class AjaxController extends FrontEndController
{
    /**
     * Filter     
     */
    public function filters()
    {
        return array('ajaxOnly');
    }        

    /**
     * Render json responce
     * @param array $json
     */
    public function renderJson($json = array())
    {
        echo json_encode($json);
        Yii::app()->end();
    }
    
    /**
     * Render html responce
     * @param string $html
     */
    public function renderHtml($html = '')
    {
        echo $html;
        Yii::app()->end();
    }
    
    /**
     * Override parent function
     */
    public function renderPartial($view,$data=null,$return=false,$processOutput=false)
    {        
        if(!Yii::app()->hasComponent('mailer') || strpos($view, Yii::app()->mailer->getPathViews()) !== 0){

            $view = '//' . str_replace('ajaxcontroller', '', strtolower(get_called_class())) . '/' . $view;
        }
        return parent::renderPartial($view, $data, $return, $processOutput);
    }
}