<?php

class Widget extends CWidget
{
    protected $clientScript;
    protected $assetsFolder = '';
    protected $script = array();
    protected $css = array();

    /**
     * Run not used...
     *
     * @return void
     */
    public function run()
    {        
    }

    /**
     * Initializes everything
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->clientScript = Yii::app()->getClientScript();
        if(file_exists(Yii::getPathOfAlias('widgets.'. get_called_class() .'.assets'))) {
            $this->assetsFolder =Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('widgets.'. get_called_class() .'.assets'), false, -1, YII_DEBUG);
            $this->registerScripts();
        }
    }
    
    public function render($view,$data=null,$return=false)
    {
        if($data === null){
            $data = array();    
        }
        
        $data = array_merge($data, array('assets_folder' => $this->assetsFolder));
        return parent::render($view,$data,$return);
    }
    
    public function renderPartial($view, array $data=NULL)
    {
        return $this->render($view, $data, TRUE);
    }        

    /**
     * Registers the JS and CSS Files
     * @return void
     */
    protected function registerScripts()
    {                                
        foreach($this->css as $file){            
            $this->clientScript->registerCssFile((strpos($file, '/') === FALSE)?$this->assetsFolder.'/css/'.$file : $file);
        }        
        foreach($this->script as $file){
            $this->clientScript->registerScriptFile((strpos($file, '/') === FALSE)?$this->assetsFolder.'/js/'.$file : $file);
        }                
    }        
}
