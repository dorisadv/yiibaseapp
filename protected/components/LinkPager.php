<?php

/**
 * Description of LinkPager
 *
 * @author hakkerok
 */
class LinkPager extends CLinkPager
{
    public $separator = '<span>&#8230;</span>';
    public $separatorClass = 'separator';

    public function createPageButtons()
    {
        if (($pageCount = $this->getPageCount()) <= 1)
            return array();

        list($beginPage, $endPage) = $this->getPageRange();
        $currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
        $buttons = array();

        // first page
        $buttons[] = $this->createPageButton($this->firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);

        // prev page
        if (($page = $currentPage - 1) < 0)
            $page = 0;
        $buttons[] = $this->createPageButton($this->prevPageLabel, $page, $this->previousPageCssClass, $currentPage <= 0, false);
        
        /*BOF - first page*/
        if($beginPage > 0) {
            $buttons[] = $this->createPageButton(1, 0, $this->internalPageCssClass, false, false);
        }
        if($beginPage > 1) {
            $buttons[] = CHtml::tag('li',array('class'=>$this->separatorClass),$this->separator);
        }
        /*EOF - first page*/

        // internal pages
        for ($i = $beginPage; $i <= $endPage; ++$i)
            $buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);
        
        /*BOF - last page*/        
        if($endPage < ($pageCount-2)) {
            $buttons[] = CHtml::tag('li',array('class'=>$this->separatorClass),$this->separator);
        }
        if($endPage < ($pageCount-1)) {
            $buttons[] = $this->createPageButton($pageCount, $pageCount-1, $this->internalPageCssClass, false, false);
        }
        /*EOF - last page*/

        // next page
        if (($page = $currentPage + 1) >= $pageCount - 1)
            $page = $pageCount - 1;
        $buttons[] = $this->createPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);

        // last page
        $buttons[] = $this->createPageButton($this->lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);

        return $buttons;
    }

}