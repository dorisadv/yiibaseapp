<?php

class UploadImageAction extends CAction
{
    public $path;

    public function run()
    {
        $this->path = rtrim($this->path, '/') . '/';
        Yii::import("ext.EAjaxUpload.qqFileUploader");        
        $allowedExtensions = array("jpg", "jpeg", "gif", "png");
        $sizeLimit = 3 * 1024 * 1024; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($this->path);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $return;
        Yii::app()->end();
    }

}