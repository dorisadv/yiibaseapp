<?php

class ResizeImageAction extends CAction
{
    public $path;    
    public $width;
    public $height;
    public $adaptive = TRUE;
    public $bgcolor = FALSE;
    
    public function run()
    {
        $this->path = rtrim($this->path, '/') . '/';
        $image = isset($_GET['image'])? $_GET['image'] : '';
        if(!empty($image) && file_exists($this->path . $image)) {            
            $func = $this->adaptive? 'adaptiveThumb' : 'thumb';
            Yii::app()->ih->load($this->path . $image)->$func($this->width, $this->height)->show();
        }
        Yii::app()->end();
    }

}