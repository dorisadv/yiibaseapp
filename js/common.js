function menuGoto(hash)
{
    var obj = null;
    switch(hash) {
        case "about": obj = $("div.block-about"); break;
        case "services": obj = $("div.block-about"); break;
        case "contacts": obj = $("div#footer"); break;
    }
    if(obj!==null && obj.length==1) { jQuery('html, body').animate({ scrollTop: obj.offset().top }, 'slow'); }
}
jQuery(function(){
    // check scroll
    if(location.hash){
        var hash = location.hash.replace("#","");
        menuGoto(hash);
    }    
    // init menu items width
    $('div.menu ul li').each(function(){
        var a = $('a',this);a.css('width', a.width()+4);
        $(this).removeClass('active-hide');
    });    
});